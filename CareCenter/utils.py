from django.contrib.auth.models import User

__author__ = 'Taras Melon'


def get_group_name(user):
    for g in user.groups.all():
        return g.name


def get_caregiver(user):
    caregiver_id = user.get_profile().giver
    return User.objects.get(id=caregiver_id)


def get_date_in_strange_format(date_in_strange_format):
    good_date = date_in_strange_format.split(' ')
    month_start = None
    if good_date[1] == 'Jan':
        month_start = "01"
    elif good_date[1] == 'Feb':
        month_start = "02"
    elif good_date[1] == 'Mar':
        month_start = "03"
    elif good_date[1] == 'Apr':
        month_start = "04"
    elif good_date[1] == 'May':
        month_start = "05"
    elif good_date[1] == 'Jun':
        month_start = "06"
    elif good_date[1] == 'Jul':
        month_start = "07"
    elif good_date[1] == 'Aug':
        month_start = "08"
    elif good_date[1] == 'Sep':
        month_start = "09"
    elif good_date[1] == 'Oct':
        month_start = "10"
    elif good_date[1] == 'Nov':
        month_start = "11"
    elif good_date[1] == 'Dec':
        month_start = "12"

    return good_date[3] + "-" + month_start + "-" + good_date[2] + " " + good_date[4]