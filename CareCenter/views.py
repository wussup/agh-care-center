# Create your views here.

# auth/views.py
from django.contrib.auth.models import User, Group
from django.core.exceptions import PermissionDenied
from django.shortcuts import render_to_response, render, redirect
from django.contrib.auth import authenticate, login, logout
from django_tables2 import RequestConfig
from django.core.mail import send_mail

from CareCenter import utils
from CareCenter.models import CheckEvent
from CareCenter.models import Event, MeasureEvent, BloodPressureEvent
from CareCenter.forms import CaregiverForm, PatientForm, EditCaregiverForm, EditPatientForm, EditEventForm
from CareCenter.tables import CaregiverTable, PatientTable
from CareCenter.utils import get_group_name


def redirect_home(user):
    group_name = None
    #we have one group
    for g in user.groups.all():
        group_name = g.name
    state = "You're successfully logged in!"
    fullname = user.first_name + ' ' + user.last_name
    group = Group.objects.get(name='patient')
    users = group.user_set.all()
    profile = None
    for new_user in users:
        if str(new_user.get_profile().user.id) == str(user.id):
            profile = new_user.get_profile()

    if profile is not None:
        caregiver_id = profile.giver
        caregiver = User.objects.get(id=caregiver_id)
        return render_to_response('home.html',
                                  {'state': state, 'group': group_name, 'fullname': fullname, 'id': user.id,
                                   'caregiver_first_name': caregiver.first_name,
                                   'caregiver_last_name': caregiver.last_name, 'caregiver_mail': caregiver.email})

    return render_to_response('home.html', {'state': state, 'group': group_name, 'fullname': fullname, 'id': user.id})


def login_user(request):
    user = request.user
    if user.is_authenticated():
        return redirect_home(user)
    else:
        state = "Please log in below..."
        username = ''
        if request.POST:
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect_home(user)
                else:
                    state = "Your account is not active, please contact the site admin."
            else:
                state = "Your username and/or password were incorrect."

        return render_to_response('auth.html', {'state': state, 'username': username})


def log_out(request):
    if request.user.is_authenticated():
        logout(request)
        return render_to_response('auth.html', {})
    else:
        raise PermissionDenied()


def add_caregiver(request):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if request.method == 'POST':  # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = CaregiverForm(request.POST)  # A form bound to the POST data
            if form.is_valid():  # All validation rules pass
                # Process the data in form.cleaned_data
                # ...
                username = form.cleaned_data['login']
                try:
                    User.objects.get(username=username)
                    return render(request, 'add_caregiver.html', {'state': form, 'submitAction': 'add',
                                                                  'ok': u'Username "%s" is already in use.' % username})
                except User.DoesNotExist:
                    password = form.cleaned_data['password']
                    first_name = form.cleaned_data['first_name']
                    last_name = form.cleaned_data['last_name']
                    email = form.cleaned_data['email']
                    created_user = User.objects.create_user(username=username, password=password, email=email)
                    created_user.first_name = first_name
                    created_user.last_name = last_name
                    created_user.save()
                    g = Group.objects.get(name='caregiver')
                    g.user_set.add(created_user)
                    form = CaregiverForm()
                    return render(request, 'add_caregiver.html',
                                  {'state': form, 'submitAction': 'add', 'ok': 'Caregiver successfully added'})
        else:
            form = CaregiverForm()  # An unbound form

        return render(request, 'add_caregiver.html', {'state': form, 'submitAction': 'add'})
    else:
        raise PermissionDenied()


def add_patient(request):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if request.method == 'POST':  # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = PatientForm(request.POST)  # A form bound to the POST data
            if form.is_valid():  # All validation rules pass
                # Process the data in form.cleaned_data
                # ...
                username = form.cleaned_data['login']
                try:
                    User.objects.get(username=username)
                    return render(request, 'add_patient.html', {'state': form, 'submitAction': 'add',
                                                                'ok': u'Username "%s" is already in use.' % username})
                except User.DoesNotExist:
                    password = form.cleaned_data['password']
                    first_name = form.cleaned_data['first_name']
                    last_name = form.cleaned_data['last_name']
                    email = form.cleaned_data['email']
                    caregiver = form.cleaned_data['caregiver']
                    created_user = User.objects.create_user(username=username, password=password, email=email)
                    created_user.first_name = first_name
                    created_user.last_name = last_name
                    created_user.save()
                    profile = created_user.get_profile()
                    profile.giver = caregiver
                    profile.save()
                    g = Group.objects.get(name='patient')
                    g.user_set.add(created_user)
                    form = PatientForm()
                    return render(request, 'add_patient.html',
                                  {'state': form, 'ok': 'Patient successfully added', 'submitAction': 'add'})
        else:
            form = PatientForm()  # An unbound form
        return render(request, 'add_patient.html', {
            'state': form, 'submitAction': 'add'
        })
    else:
        raise PermissionDenied()


def caregivers(request):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        group = Group.objects.get(name='caregiver')
        users = group.user_set.all()
        table = CaregiverTable(users)
        RequestConfig(request).configure(table)
        return render(request, 'caregivers.html', {'state': table})
    else:
        raise PermissionDenied()


def patients(request):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        profiles = []
        for user in users:
            profiles.append(user.get_profile())

        table = PatientTable(profiles, get_group_name(request.user))
        RequestConfig(request).configure(table)
        return render(request, 'patients.html', {'state': table})
    else:
        raise PermissionDenied()


def caregiver_details(request, car_id=None):
    if request.user.is_authenticated() and get_group_name(request.user) != 'patient':
        if car_id is None:
            return
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        profiles = []
        for user in users:
            profile = user.get_profile()
            if str(profile.giver) == str(car_id):
                profiles.append(profile)

        table = PatientTable(profiles, get_group_name(request.user))
        RequestConfig(request).configure(table)
        return render(request, 'patients.html', {'state': table})
    else:
        raise PermissionDenied()


def patient_details(request, pat_id=None):
    if request.user.is_authenticated():
        if pat_id is None:
            return
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        profiles = []
        for user in users:
            profile = user.get_profile()
            if profile.giver == pat_id:
                profiles.append(profile)
        #delete all events
        #Event.objects.all().delete()
        entries = Event.objects.filter(patient_id=pat_id)
        json_list = []
        for entry in entries:
            title = entry.title
            start = entry.startDate.strftime("%Y-%m-%dT%H:%M:%S")
            end = entry.endDate.strftime("%Y-%m-%dT%H:%M:%S")
            entry_id = entry.id
            all_day = entry.allDay
            event_type = entry.type
            if all_day:
                all_day = 1
            else:
                all_day = 0
            value1 = ''
            value2 = ''
            if event_type == '1':
                #entry.__class__ = CheckEvent
                check_events = CheckEvent.objects.all()
                prof = None
                for ev in check_events:
                    if str(ev.id) == str(entry.id):
                        prof = ev
                if prof.check:
                    value1 = '1'.encode('utf-8')
                else:
                    value1 = '0'.encode('utf-8')
            elif event_type == '2':
                #entry.__class__ = MeasureEvent
                measure_events = MeasureEvent.objects.all()
                prof = None
                for ev in measure_events:
                    if str(ev.id) == str(entry.id):
                        prof = ev

                value1 = prof.measureValue.encode('utf-8')
            elif event_type == '3':
                #entry.__class__ = BloodPressureEvent
                blood_events = BloodPressureEvent.objects.all()
                prof = None
                for ev in blood_events:
                    if str(ev.id) == str(entry.id):
                        prof = ev
                value1 = prof.measureValue1
                value2 = prof.measureValue2
            json_entry = {'start': start, 'end': end, 'title': title.encode('utf-8'), 'allDay': all_day, 'id': entry_id,
                          'type': event_type.encode('utf-8'), 'value1': value1,
                          'value2': value2}
            json_list.append(json_entry)
        #print json_list
        user = User.objects.get(id=pat_id)
        if utils.get_group_name(request.user) == 'caregiver':
            editable = 1
        else:
            editable = 0
        return render(request, 'calendar_plan.html',
                      {'events': json_list, 'fullname': user.first_name + ' ' + user.last_name, 'editable': editable,
                       'perm': utils.get_group_name(request.user)})
    else:
        raise PermissionDenied()


def save_event(request, pat_id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'patient':
        # ContactForm was defined in the the previous section
        event_id = request.POST.get('id')  # A form bound to the POST data
        event_type = request.POST.get('type')
        if event_type == '1':
            checked = request.POST.get('value')
            if checked == 'Done':
                checked = True
            else:
                checked = False
            #check_events = CheckEvent.objects.all()
            events = Event.objects.all()
            for event in events:
                if event.type == '1':
                    if str(event.id) == str(event_id):
                        event.__class__ = CheckEvent
                        event.check = checked
                        event.save()
        elif event_type == '2':
            value = request.POST.get('value')

            events = Event.objects.all()
            for event in events:
                if event.type == '2':
                    if str(event.id) == str(event_id):
                        event.__class__ = MeasureEvent
                        measure = MeasureEvent.objects.get(id=event_id)
                        measure.measureValue = value
                        measure.save()
                        group = Group.objects.get(name='patient')
                        users = group.user_set.all()
                        for user in users:
                            if user.id == int(event.patient_id):
                                if (value < measure.minValue or value > measure.maxValue):
                                    send_mail('AGH Care Center - Patient ALERT!',
                                              'Message was generated by AGH Care Center System.\n\n' +
                                              'Patient: ' + user.first_name + ' ' + user.last_name + '.\n\nPatient has bad result of:  ' + event.title +
                                              '\nNormal value should be between: ' + measure.minValue + '-' + measure.maxValue +
                                              '\n\n Patient measure is: ' + measure.measureValue, 'AGH Care Center',
                                              ['kolotek13@gmail.com'], fail_silently=True)



        elif event_type == '3':
            value1 = request.POST.get('value1')
            value2 = request.POST.get('value2')
            events = Event.objects.all()
            for event in events:
                if event.type == '3':
                    if str(event.id) == str(event_id):
                        event.__class__ = BloodPressureEvent
                        bloodPressure = BloodPressureEvent.objects.get(id=event_id)
                        bloodPressure.measureValue1 = value1
                        bloodPressure.measureValue2 = value2
                        bloodPressure.save()
                        group = Group.objects.get(name='patient')
                        users = group.user_set.all()
                        for user in users:
                            if user.id == int(event.patient_id):
                                if (
                                                        value1 < bloodPressure.minBloodValue or value1 > bloodPressure.maxBloodValue or value2 < bloodPressure.secMinValue or value2 > bloodPressure.secMaxValue):
                                    send_mail('AGH Care Center - Patient ALERT!',
                                              'Message was generated by AGH Care Center System.\n\n' +
                                              'Patient: ' + user.first_name + ' ' + user.last_name + '.\n\nPatient has bad result of blood pressure:  ' + event.title +
                                              '\nNormal value should be between: (' + bloodPressure.minBloodValue + '-' + bloodPressure.maxBloodValue + ') / (' +
                                              bloodPressure.secMinValue + '-' + bloodPressure.secMaxValue + ')'
                                              + '\n\n Patient measure is: ' + bloodPressure.measureValue1 + ' / ' + bloodPressure.measureValue2,
                                              'AGH Care Center',
                                              ['praca.taras@gmail.com'], fail_silently=True)

        return redirect('/patients/' + pat_id + '/')
    else:
        raise PermissionDenied()


def add_event(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'caregiver':
        if id is None:
            raise PermissionDenied()
        else:
            title = request.POST.get('title')
            allDay = request.POST.get('allDay')
            if allDay == 'true':
                allDay = True
            else:
                allDay = False

            startDate = utils.get_date_in_strange_format(request.POST.get('start'))
            endDate = utils.get_date_in_strange_format(request.POST.get('end'))
            type = request.POST.get('type')
            if (type == 'Standard'):
                CheckEvent.objects.create(title=title, startDate=startDate, endDate=endDate, allDay=allDay,
                                          patient_id=id, type='1', check=False).save()
            elif type == 'Measure':
                min = request.POST.get('min')
                max = request.POST.get('max')
                MeasureEvent.objects.create(title=title, startDate=startDate, endDate=endDate, allDay=allDay,
                                            patient_id=id, type='2', measureValue='', minValue=min, maxValue=max).save()
            else:
                min = request.POST.get('min')
                max = request.POST.get('max')
                secMin = request.POST.get('secMin')
                secMax = request.POST.get('secMax')
                BloodPressureEvent.objects.create(title=title, startDate=startDate, endDate=endDate, allDay=allDay,
                                                  patient_id=id, type='3', measureValue1='0', measureValue2='0',
                                                  minBloodValue=min, maxBloodValue=max, secMinValue=secMin,
                                                  secMaxValue=secMax).save()
            return patient_details(request, id)
    else:
        raise PermissionDenied()


def update_drop_event(request, id=None):
    event_id = request.POST.get('id')
    event_to_change = Event.objects.get(id=event_id)

    allDay = str(request.POST.get('allDay'))
    if allDay == 'true' or allDay == '1':
        event_to_change.allDay = True
        event_to_change.endDate = utils.get_date_in_strange_format(request.POST.get('start'))
    else:
        event_to_change.allDay = False
    if (request.POST.get('end') != '' and str(request.POST.get('allDay')) != 'true' and str(
            request.POST.get('allDay')) != '1'):
        event_to_change.endDate = utils.get_date_in_strange_format(request.POST.get('end'))

    event_to_change.startDate = utils.get_date_in_strange_format(request.POST.get('start'))

    event_to_change.save()


def delete_caregiver(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if id is None:
            return
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        for user in users:
            profile = user.get_profile()
            if profile.giver == id:
                profile.giver = None

        user = User.objects.get(id=id)
        user.delete()
        group = Group.objects.get(name='caregiver')
        users = group.user_set.all()
        table = CaregiverTable(users)
        RequestConfig(request).configure(table)
        return redirect('/caregivers/')
    else:
        raise PermissionDenied()


def delete_patient(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if id is None:
            return
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        for user in users:
            if str(user.get_profile().user.id) == str(id):
                user.delete()
        group = Group.objects.get(name='patient')
        users = group.user_set.all()
        profiles = []
        for user in users:
            profiles.append(user.get_profile())

        table = PatientTable(profiles, get_group_name(request.user))
        RequestConfig(request).configure(table)
        return redirect('/patients/')
    else:
        raise PermissionDenied()


def edit_caregiver(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if id is None:
            return
        else:
            user = User.objects.get(id=id)
            first_name = user.first_name
            last_name = user.last_name
            email = user.email
            form = EditCaregiverForm()
            form.fields['first_name'].initial = first_name
            form.fields['last_name'].initial = last_name
            form.fields['email'].initial = email
            return render(request, 'add_caregiver.html', {'state': form, 'submitAction': 'edit', 'id': id})
    else:
        raise PermissionDenied()


def success_edit_caregiver(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        # ContactForm was defined in the the previous section
        form = EditCaregiverForm(request.POST)  # A form bound to the POST data
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            user = User.objects.get(id=id)
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.save()
            return redirect('/caregivers/')

        return render(request, 'add_caregiver.html', {'state': form, 'submitAction': 'edit', 'id': id})
    else:
        raise PermissionDenied()


def edit_patient(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        if id is None:
            return
        else:
            group = Group.objects.get(name='patient')
            users = group.user_set.all()
            profile = None
            for user in users:
                if str(user.get_profile().user.id) == str(id):
                    profile = user.get_profile()

            first_name = profile.user.first_name
            last_name = profile.user.last_name
            email = profile.user.email
            caregiver = profile.giver
            form = EditPatientForm()
            form.fields['first_name'].initial = first_name
            form.fields['last_name'].initial = last_name
            form.fields['email'].initial = email
            form.fields['caregiver'].initial = caregiver
            return render(request, 'add_patient.html', {'state': form, 'submitAction': 'edit', 'id': id})
    else:
        raise PermissionDenied()


def success_edit_patient(request, id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'admin':
        # ContactForm was defined in the the previous section
        form = EditPatientForm(request.POST)  # A form bound to the POST data
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            caregiver = form.cleaned_data['caregiver']
            group = Group.objects.get(name='patient')
            users = group.user_set.all()
            profile = None
            for user in users:
                if str(user.get_profile().user.id) == str(id):
                    profile = user.get_profile()
            profile.user.first_name = first_name
            profile.user.last_name = last_name
            profile.user.email = email
            profile.user.save()
            profile.giver = caregiver
            profile.save()

            return redirect('/patients/')
        return render(request, 'add_patient.html', {'state': form, 'submitAction': 'edit', 'id': id})
    else:
        raise PermissionDenied()


def delete_event(request, user_id=None, event_id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'caregiver':
        if user_id is None or event_id is None:
            raise PermissionDenied()
        events = Event.objects.all()
        for event in events:
            if str(event_id) == str(event.id) and str(event.patient_id) == str(user_id):
                event.delete()
        return redirect('/patients/' + str(user_id) + '/')
    else:
        raise PermissionDenied()


def edit_event(request, user_id=None, event_id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'caregiver':
        if user_id is None or event_id is None:
            raise PermissionDenied()
        else:
            events = Event.objects.all()
            profile = None
            for event in events:
                if str(event.id) == str(event_id) and str(event.patient_id) == str(user_id):
                    profile = event

            form = EditEventForm()
            form.fields['title'].initial = profile.title
            form.fields['type'].initial = profile.type
            return render(request, 'edit_event.html', {'state': form, 'id': user_id, 'event_id': event_id})
    else:
        raise PermissionDenied()


def success_edit_event(request, user_id=None, event_id=None):
    if request.user.is_authenticated() and get_group_name(request.user) == 'caregiver':
        # ContactForm was defined in the the previous section
        form = EditEventForm(request.POST)  # A form bound to the POST data
        if form.is_valid():
            title = form.cleaned_data['title']
            event_type = form.cleaned_data['type']
            events = Event.objects.all()
            profile = None
            for event in events:
                if str(event.id) == str(event_id) and str(event.patient_id) == str(user_id):
                    profile = event
            profile.title = title
            profile.type = event_type
            if event_type == '1':
                profile.__class__ = CheckEvent
                profile.check = False
            elif event_type == '2':
                profile.__class__ = MeasureEvent
                profile.measureValue = ''
            else:
                profile.__class__ = BloodPressureEvent
                profile.measureValue1 = 0
                profile.measureValue2 = 0
            profile.save()

            return redirect('/patients/' + user_id + '/')
        return render(request, 'edit_event.html', {'state': form, 'id': id, 'event_id': event_id})
    else:
        raise PermissionDenied()
