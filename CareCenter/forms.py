from django.contrib.auth.models import Group
from django import forms

from CareCenter.models import CaregiverModel, PatientModel, Event


__author__ = 'Taras Melon'


class CaregiverForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = CaregiverModel


class EditCaregiverForm(forms.ModelForm):
    class Meta:
        model = CaregiverModel
        exclude = ('password', 'login')


class PatientForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    caregiver = forms.ChoiceField()

    def __init__(self, *args, **kwargs):
        super(PatientForm, self).__init__(*args, **kwargs)
        group = Group.objects.get(name='caregiver')
        users = group.user_set.all()
        choices = []
        for user in users:
            choices.append((user.id, user.first_name + ' ' + user.last_name))
        self.fields["caregiver"].choices = choices

    class Meta:
        model = PatientModel


class EditPatientForm(forms.ModelForm):
    caregiver = forms.ChoiceField()

    def __init__(self, *args, **kwargs):
        super(EditPatientForm, self).__init__(*args, **kwargs)
        group = Group.objects.get(name='caregiver')
        users = group.user_set.all()
        choices = []
        for user in users:
            choices.append((user.id, user.first_name + ' ' + user.last_name))
        self.fields["caregiver"].choices = choices

    class Meta:
        model = PatientModel
        exclude = ('password', 'login')


CHOICES = (('1', 'Standard'),
           ('2', 'Measure'),
           ('3', 'BloodPressure'))


class EditEventForm(forms.ModelForm):
    type = forms.ChoiceField()

    class Meta:
        model = Event
        exclude = ('patient_id', 'startDate', 'endDate', 'allDay',)

    def __init__(self, *args, **kwargs):
        super(EditEventForm, self).__init__(*args, **kwargs)
        self.fields["type"].choices = CHOICES