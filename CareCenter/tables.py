from django.contrib.auth.models import User
from django_tables2 import tables, A, LinkColumn

from CareCenter.models import UserProfile


__author__ = 'Taras Melon'


class CustomTextLinkColumn(LinkColumn):
    def __init__(self, viewname, urlconf=None, args=None,
                 kwargs=None, current_app=None, attrs=None, custom_text=None, **extra):
        super(CustomTextLinkColumn, self).__init__(viewname, urlconf=urlconf,
                                                   args=args, kwargs=kwargs, current_app=current_app, attrs=attrs,
                                                   **extra)
        self.custom_text = custom_text

    def render(self, value, record, bound_column):
        try:
            user = User.objects.get(id=value)
            self.custom_text = user.first_name + ' ' + user.last_name
            return super(CustomTextLinkColumn, self).render(
                self.custom_text if self.custom_text else value,
                record, bound_column)
        except:
            self.custom_text = '-'
            return self.custom_text


class CaregiverTable(tables.Table):
    id = tables.columns.Column(visible=False)
    first_name = tables.columns.Column()
    last_name = tables.columns.Column()
    username = tables.columns.LinkColumn('caregiver_details', args={A('id')}, empty_values=())
    edit_user = tables.columns.TemplateColumn(
        '<center><a href="/caregivers/{{record.id}}/edit/"><img src="{{ MEDIA_URL }}icon_edit.png" '
        'alt="Edit" width=12 length=12></a></center>', orderable=False, verbose_name="Edit")
    delete_user = tables.columns.TemplateColumn(
        '<center><a href="/caregivers/{{record.id}}/delete/"><img src="{{ MEDIA_URL }}icon_delete.png" '
        'alt="Delete" width=12 length=12></a></center>', orderable=False, verbose_name="Delete")

    class Meta:
        model = User
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}
        fields = ('id', 'username', 'first_name', 'last_name', 'email')


class PatientTable(tables.Table):
    id = tables.columns.Column(visible=False, accessor='user.id')
    username = tables.columns.LinkColumn('patient_details', args=[A('user.id')],
                                         accessor='user.username', empty_values=())
    first_name = tables.columns.Column(accessor='user.first_name')
    last_name = tables.columns.Column(accessor='user.last_name')
    email = tables.columns.EmailColumn(accessor='user.email')
    giver = CustomTextLinkColumn('caregiver_details', args=[A('giver')],
                                 verbose_name="Caregiver", accessor='giver', empty_values=())
    user = tables.columns.Column(visible=False)
    edit_user = tables.columns.TemplateColumn(
        '<center><a href="/patients/{{record.user.id}}/edit/"><img src="{{ MEDIA_URL }}icon_edit.png" '
        'alt="Edit" width=12 length=12></a></center>', orderable=False, verbose_name="Edit")
    delete_user = tables.columns.TemplateColumn(
        '<center><a href="/patients/{{record.user.id}}/delete/"><img src="{{ MEDIA_URL }}icon_delete.png" '
        'alt="Delete" width=12 length=12></a></center>', orderable=False, verbose_name="Delete")

    def __init__(self, values, group='caregiver', *args, **kwargs):
        super(PatientTable, self).__init__(values, *args, **kwargs)
        if group == 'caregiver':
            self.columns['edit_user'].column.visible = False
            self.columns['delete_user'].column.visible = False

    class Meta:
        model = UserProfile
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}
        sequence = ('username', 'first_name', 'last_name', 'email', 'giver')