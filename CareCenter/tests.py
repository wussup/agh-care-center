# Create your tests here.
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group, User
from django.test import Client
from django.utils import unittest

from CareCenter.forms import CaregiverForm, PatientForm, EditCaregiverForm, EditPatientForm
from CareCenter.utils import get_group_name
from CareCenter.views import redirect_home


class MyTest(unittest.TestCase):
    def setUp(self):
        admin_group = Group.objects.get_or_create(name='admin')[0]
        admin_group.save()
        caregiver_group = Group.objects.get_or_create(name='caregiver')[0]
        caregiver_group.save()
        patient_group = Group.objects.get_or_create(name='patient')[0]
        patient_group.save()
        taras = User.objects.get_or_create(username='taras', password='melon', email='tmelon@wp.pl')[0]
        taras.set_password('melon')
        taras.save()
        admin_group.user_set.add(taras)
        taras.save()
        admin_group.save()
        patient = User.objects.get_or_create(username='patient', password='patient', email='patient@wp.pl')[0]
        patient.set_password('patient')
        patient.save()
        patient_group.user_set.add(patient)
        patient.save()
        patient_group.save()
        caregiver = User.objects.get_or_create(username='caregiver', password='caregiver', email='caregiver@wp.pl')[
            0]
        caregiver.set_password('caregiver')
        caregiver.save()
        caregiver_group.user_set.add(caregiver)
        caregiver.save()
        caregiver_group.save()
        patient.get_profile().giver = caregiver.id
        patient.get_profile().save()
        patient.save()

    def tearDown(self):
        taras = User.objects.get(username='taras')
        taras.delete()
        try:
            caregiver = User.objects.get(username='caregiver')
            caregiver.delete()
        except:
            #ignore
            print ''
        try:
            patient = User.objects.get(username='patient')
            patient.delete()
        except:
            #ignore
            print ''
        admin_group = Group.objects.get(name='admin')
        admin_group.delete()
        caregiver_group = Group.objects.get(name='caregiver')
        caregiver_group.delete()
        patient_group = Group.objects.get(name='patient')
        patient_group.delete()

    def test_login_logout(self):
        c = Client()
        #admin
        response = c.post('/', {'username': 'taras', 'password': 'melon'})
        self.assertEquals(response.status_code, 200)
        response = c.get('/logout/')
        #302 - good redirect
        self.assertEquals(response.status_code, 302)
        #caregiver
        response = c.post('/', {'username': 'caregiver', 'password': 'caregiver'})
        self.assertEquals(response.status_code, 200)
        response = c.get('/logout/')
        #302 - good redirect
        self.assertEquals(response.status_code, 302)
        response = c.post('/', {'username': 'patient', 'password': 'patient'})
        self.assertEquals(response.status_code, 200)
        response = c.get('/logout/')
        #302 - good redirect
        self.assertEquals(response.status_code, 302)

    def test_admin_views(self):
        c = Client()
        #admin
        response = c.post('/', {'username': 'taras', 'password': 'melon'})
        self.assertEquals(response.status_code, 200)
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        response = c.get('/caregivers/add/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/patients/add/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/caregivers/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/patients/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)

    def test_caregiver_views(self):
        c = Client()
        #caregiver
        response = c.post('/', {'username': 'caregiver', 'password': 'caregiver'})
        self.assertEquals(response.status_code, 200)
        authenticate(username='caregiver', password='caregiver')
        self.assertTrue(c.login(username='caregiver', password='caregiver'))
        user = User.objects.get(username='caregiver')
        response = c.get('/caregivers/' + str(user.id) + '/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)

    def test_bad_login(self):
        c = Client()
        response = c.post('/', {'username': 'caregiver', 'password': 'car'})
        self.assertEquals(response.status_code, 200)
        self.assertTrue("Your username and/or password were incorrect." in str(response))

    def test_not_active_user_login(self):
        c = Client()
        user = User.objects.get(username='caregiver')
        user.is_active = False
        user.save()
        response = c.post('/', {'username': 'caregiver', 'password': 'caregiver'})
        self.assertEquals(response.status_code, 200)
        self.assertTrue("Your account is not active, please contact the site admin." in str(response))
        user.is_active = True
        user.save()

    def test_caregiver_form(self):
        #invalid form
        data = {
            'login': 'car',
            'password': 'car',
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car'
        }
        form = CaregiverForm(data=data)
        self.assertFalse(form.is_valid())
        #valid form
        data = {
            'login': 'car',
            'password': 'car',
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car@wp.pl'
        }
        form = CaregiverForm(data=data)
        self.assertTrue(form.is_valid())

    def test_patient_form(self):
        #invalid form
        data = {
            'login': 'pat',
            'password': 'pat',
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat',
            'caregiver': '1'
        }
        form = PatientForm(data=data)
        self.assertFalse(form.is_valid())
        #valid form
        user = User.objects.get(username='caregiver')
        data = {
            'login': 'pat',
            'password': 'pat',
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat@wp.pl',
            'caregiver': user.id
        }
        form = PatientForm(data=data)
        self.assertTrue(form.is_valid())

    def test_edit_caregiver_form(self):
        #invalid form
        data = {
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car'
        }
        form = EditCaregiverForm(data=data)
        self.assertFalse(form.is_valid())
        #valid form
        data = {
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car@wp.pl'
        }
        form = EditCaregiverForm(data=data)
        self.assertTrue(form.is_valid())

    def test_edit_patient_form(self):
        #invalid form
        data = {
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat',
            'caregiver': '1'
        }
        form = EditPatientForm(data=data)
        self.assertFalse(form.is_valid())
        #valid form
        user = User.objects.get(username='caregiver')
        data = {
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat@wp.pl',
            'caregiver': user.id
        }
        form = EditPatientForm(data=data)
        self.assertTrue(form.is_valid())

    def test_custom_text_link_column(self):
        c = Client()
        #admin
        response = c.post('/', {'username': 'taras', 'password': 'melon'})
        self.assertEquals(response.status_code, 200)
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        response = c.get('/patients/')
        self.assertEquals(response.status_code, 200)
        patient = User.objects.get(username='patient')
        patient.get_profile().giver = None
        patient.get_profile().save()
        patient.save()
        response = c.get('/patients/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_get_group_name(self):
        name = 'caregiver'
        user = User.objects.get(username=name)
        self.assertEquals(name, get_group_name(user))

    def test_redirect_home(self):
        name = 'caregiver'
        user = User.objects.get(username=name)
        self.assertTrue("Home" in str(redirect_home(user)))
        name = 'patient'
        patient = User.objects.get(username='patient')
        patient.get_profile().giver = User.objects.get(username='caregiver').id
        patient.get_profile().save()
        patient.save()
        user = User.objects.get(username=name)
        self.assertTrue("Home" in str(redirect_home(user)))

    def test_add_caregiver(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        #valid form
        data = {
            'login': 'car',
            'password': 'car',
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car@wp.pl'
        }
        form = CaregiverForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/caregivers/add/', data=data)
        self.assertEquals(response.status_code, 200)
        User.objects.get(username='car')
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_add_bad_caregiver(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        #valid form
        data = {
            'login': 'caregiver',
            'password': 'car',
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car@wp.pl'
        }
        form = CaregiverForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/caregivers/add/', data=data)
        self.assertEquals(response.status_code, 200)
        self.assertTrue("is already in use" in str(response))
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_add_patient(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        #valid form
        user = User.objects.get(username='caregiver')
        data = {
            'login': 'pat',
            'password': 'pat',
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat@wp.pl',
            'caregiver': user.id
        }
        form = PatientForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/patients/add/', data=data)
        self.assertEquals(response.status_code, 200)
        User.objects.get(username='pat')
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_add_bad_patient(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        #valid form
        user = User.objects.get(username='caregiver')
        data = {
            'login': 'patient',
            'password': 'pat',
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat@wp.pl',
            'caregiver': user.id
        }
        form = PatientForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/patients/add/', data=data)
        self.assertEquals(response.status_code, 200)
        self.assertTrue("is already in use" in str(response))
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_patient_details(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        #patient
        user = User.objects.get(username='patient')
        response = c.get('/patients/' + str(user.id) + '/')
        self.assertEquals(response.status_code, 200)
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_delete_caregiver(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        user = User.objects.get(username='caregiver')
        response = c.get('/caregivers/' + str(user.id) + '/delete/')
        self.assertEquals(response.status_code, 302)
        try:
            User.objects.get(username='caregiver')
            self.assertTrue(False)
        except:
            self.assertTrue(True)
        c.logout()

    def test_delete_patient(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        user = User.objects.get(username='patient')
        response = c.get('/patients/' + str(user.id) + '/delete/')
        self.assertEquals(response.status_code, 302)
        try:
            User.objects.get(username='patient')
            self.assertTrue(False)
        except:
            self.assertTrue(True)
        c.logout()

    def test_edit_caregiver(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        user = User.objects.get(username='caregiver')
        response = c.get('/caregivers/' + str(user.id) + '/edit/')
        self.assertEquals(response.status_code, 200)
        #valid form
        data = {
            'first_name': 'car',
            'last_name': 'car',
            'email': 'car@wp.pl'
        }
        form = EditCaregiverForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/caregivers/' + str(user.id) + '/success/', data=data)
        self.assertEquals(response.status_code, 302)
        user = User.objects.get(username='caregiver')
        self.assertEquals(user.first_name, data['first_name'])
        user.delete()
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_edit_patient(self):
        c = Client()
        authenticate(username='taras', password='melon')
        self.assertTrue(c.login(username='taras', password='melon'))
        user = User.objects.get(username='patient')
        response = c.get('/patients/' + str(user.id) + '/edit/')
        self.assertEquals(response.status_code, 200)
        #valid form
        data = {
            'first_name': 'pat',
            'last_name': 'pat',
            'email': 'pat@wp.pl',
            'caregiver': User.objects.get(username='caregiver').id
        }
        form = EditPatientForm(data=data)
        self.assertTrue(form.is_valid())
        response = c.post('/patients/' + str(user.id) + '/success/', data=data)
        self.assertEquals(response.status_code, 302)
        user = User.objects.get(username='patient')
        self.assertEquals(user.get_profile().user.first_name, data['first_name'])
        user.delete()
        response = c.get('/')
        self.assertEquals(response.status_code, 200)
        c.logout()

    def test_forbidden(self):
        c = Client()
        response = c.get('/caregivers/add/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/add/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/caregivers/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/')
        self.assertEquals(response.status_code, 403)
        caregiver = User.objects.get(username='caregiver')
        patient = User.objects.get(username='patient')
        response = c.get('/caregivers/' + str(caregiver.id) + '/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/' + str(patient.id) + '/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/caregivers/' + str(caregiver.id) + '/delete/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/' + str(patient.id) + '/delete/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/caregivers/' + str(caregiver.id) + '/edit/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/' + str(patient.id) + '/edit/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/caregivers/' + str(caregiver.id) + '/success/')
        self.assertEquals(response.status_code, 403)
        response = c.get('/patients/' + str(patient.id) + '/success/')
        self.assertEquals(response.status_code, 403)

    def test_add_event(self):
        c = Client()
        authenticate(username='caregiver', password='caregiver')
        self.assertTrue(c.login(username='caregiver', password='caregiver'))
        user = User.objects.get(username='patient')
        response = c.post('/patients/' + str(user.id) + '/add_event/',
                          {'title': 'HelloTest', 'allDay': 'true', 'start': 'Wed May 21 2014 00:00:00',
                           'end': 'Wed May 21 2014 00:00:00'})
        self.assertEquals(response.status_code, 200)
        response = c.get('/patients/' + str(user.id) + '/')
        self.assertEquals(response.status_code, 200)
        self.assertTrue("HelloTest" in str(response))




