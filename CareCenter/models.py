from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save


class CaregiverModel(models.Model):
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=50)


class PatientModel(models.Model):
    login = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=50)
    caregiver = models.CharField(max_length=100)


class Event(models.Model):
    title = models.CharField(max_length=30)
    startDate = models.DateTimeField()
    endDate = models.DateTimeField()
    allDay = models.BooleanField()
    patient_id = models.CharField(max_length=30)
    type = models.CharField(max_length=100)


class CheckEvent(Event):
    check = models.BooleanField()


class MeasureEvent(Event):
    measureValue = models.CharField(max_length=20)
    minValue = models.CharField(max_length=20)
    maxValue = models.CharField(max_length=20)


class BloodPressureEvent(Event):
    measureValue1 = models.IntegerField(max_length=3)
    measureValue2 = models.IntegerField(max_length=3)
    minBloodValue = models.CharField(max_length=20)
    maxBloodValue = models.CharField(max_length=20)
    secMinValue = models.CharField(max_length=20)
    secMaxValue = models.CharField(max_length=20)


class UserProfile(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    # Other fields here
    giver = models.IntegerField(null=True, blank=True)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


post_save.connect(create_user_profile, sender=User)