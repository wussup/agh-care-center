from django.conf.urls import patterns, include, url
from django.contrib import admin

from iosr import settings


admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'iosr.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       (r'^$', 'CareCenter.views.login_user'),
                       url(r'^admin/', include(admin.site.urls)),
                       (r'^caregivers/add', 'CareCenter.views.add_caregiver'),
                       (r'^patients/add', 'CareCenter.views.add_patient'),
                       url(regex=r'^caregivers/(?P<id>\d+)/delete/$', view='CareCenter.views.delete_caregiver'),
                       url(regex=r'^caregivers/(?P<id>\d+)/edit/$', view='CareCenter.views.edit_caregiver'),
                       url(regex=r'^caregivers/(?P<id>\d+)/success/$', view='CareCenter.views.success_edit_caregiver'),
                       url(regex=r'^patients/(?P<id>\d+)/delete/$', view='CareCenter.views.delete_patient'),
                       url(regex=r'^patients/(?P<id>\d+)/edit/$', view='CareCenter.views.edit_patient'),
                       url(regex=r'^patients/(?P<id>\d+)/success/$', view='CareCenter.views.success_edit_patient'),
                       url(regex=r'^caregivers/(?P<id>\d+)/$', view='CareCenter.views.caregiver_details',
                           name='caregiver_details'),
                       url(regex=r'^patients/(?P<id>\d+)/save_event/$', view='CareCenter.views.save_event'),
                       url(regex=r'^patients/(?P<id>\d+)/$', view='CareCenter.views.patient_details',
                           name='patient_details'),
                       url(regex=r'^patients/(?P<id>\d+)/add_event/$', view='CareCenter.views.add_event',
                           name='add_event'),
                       url(regex=r'^patients/(?P<id>\d+)/update_drop_event/$',
                           view='CareCenter.views.update_drop_event',
                           name='update_drop_event'),
                       url(regex=r'^patients/(?P<user_id>\d+)/events/(?P<event_id>\d+)/delete/$',
                           view='CareCenter.views.delete_event'),
                       url(regex=r'^patients/(?P<user_id>\d+)/events/(?P<event_id>\d+)/success/$',
                           view='CareCenter.views.success_edit_event'),
                       url(regex=r'^patients/(?P<user_id>\d+)/events/(?P<event_id>\d+)/$',
                           view='CareCenter.views.edit_event'),

                       (r'^caregivers', 'CareCenter.views.caregivers'),
                       (r'^patients', 'CareCenter.views.patients'),
                       (r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                        {'document_root': settings.MEDIA_ROOT}),
)
